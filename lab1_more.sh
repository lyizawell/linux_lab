#!/bin/sh
read path

max=0

for i in $(find -type d);
do
if [ -f $i ];
then
	continue
fi
	tmp=$(ls -f $i | wc -l)
	echo $i $tmp
	if [ $max -le $tmp ];
	then
		max=$tmp
	fi
done 

echo $max
